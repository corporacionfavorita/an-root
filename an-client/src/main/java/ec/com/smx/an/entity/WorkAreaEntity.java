package ec.com.smx.an.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Work Area entity.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTAREATRABAJO")
public class WorkAreaEntity {

    @Id
    @Column(name = "CODIGOAREATRABAJO")
    private Integer workAreaCode;

    @Column(name = "CODIGOCOMPANIA")
    private Integer companyCode;

    @Column(name = "NOMBREAREATRABAJO")
    private String name;

    @Column(name = "ESTADOAREATRABAJO")
    private String status;

    @Column(name = "CODIGOREFERENCIA")
    private Integer referenceCode;

    @Column(name = "TIPOAREATRABAJOVALOR")
    private String typeWorkAreaValue;




}
