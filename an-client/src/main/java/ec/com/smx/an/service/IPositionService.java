package ec.com.smx.an.service;

import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.vo.PositionVo;
import java.util.List;

/**
 * Position services specification.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
public interface IPositionService extends IBaseService<PositionEntity> {

    /**
     * Find all position list.
     *
     * @return An array of positions
     */
    List<PositionVo> list();

    /**
     * Find all position list by work Area.
     *
     * @return An array of positions
     */
    List<PositionVo> listByWorkArea(Integer workAreaCode);

    /**
     * Find all position list by work Area.
     *
     * @return An array of positions
     */
    PositionVo positionPersonList(Integer workAreaCode,Integer positionCode);

    /**
     * Find all position list by work Area.
     *
     * @return An array of positions
     */
    PositionVo fidByCode(Integer positionCode);
}
