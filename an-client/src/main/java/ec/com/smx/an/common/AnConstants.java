package ec.com.smx.an.common;

/**
 * Global Constants specification.
 *
 * @author scuascota on 2021/08/23.
 * @version 1.0
 */
public final class AnConstants {

    public static final String URL_GET_CATALOG_LIST = "/datosCorporativo/server/getCatalogList/";

    public static final String CONSOLE_ENV = "CONSOLA";

    private AnConstants(){
        super();
    }

}
