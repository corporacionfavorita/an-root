package ec.com.smx.an.exception;

/**
 * An Runtime Exception Class.
 *
 * @author evacacela on 2021/09/18.
 * @version 1.0.0
 */
public class AnRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 3263046821289003394L;

    /**
     * Constructor with args.
     *
     * @param message The message
     * @param cause The cause
     */
    public AnRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with args.
     *
     * @param message The message
     */
    public AnRuntimeException(String message) {
        super(message);
    }

}
