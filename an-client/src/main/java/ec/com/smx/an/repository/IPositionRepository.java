package ec.com.smx.an.repository;

import ec.com.kruger.spring.orm.repository.IQueryDslBaseRepository;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.vo.PositionVo;
import java.util.List;

/**
 * Position repository specification.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
public interface IPositionRepository extends IQueryDslBaseRepository<PositionEntity> {

    /**
     * Find all position list.
     *
     * @return An array of positions
     */
    List<PositionVo> list();

    /**
     * Find all position list by work Area.
     *
     * @return An array of positions
     */
    List<PositionVo> listByWorkArea(Integer workAreaCode);

    /**
     * Find Work Area by code.
     *
     * @return An array of Work Area's
     */
    PositionVo fidByCode(Integer positionCode);

}
