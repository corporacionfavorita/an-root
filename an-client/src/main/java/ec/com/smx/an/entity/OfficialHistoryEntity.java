package ec.com.smx.an.entity;

import lombok.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

/**
 * Functionary History entity.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SPRADTEMPHIS")
public class OfficialHistoryEntity {

    @Id
    @Column(name = "SECUENCIALHISTORIALEMPLEADO")
    private Integer employeeSequenceCode;

    @Column(name = "CODIGOREFERENCIAEMPLEADO")
    private Integer employeeReferenceCode;

    @Column(name = "CODIGOCOMPANIA")
    private Integer companyCode;

    @Column(name = "CODIGOFUNCIONARIO")
    private String officialCode;

    @Column(name = "FECHAFIN")
    private Date endDate;

    @Column(name = "CODIGOCARGO")
    private Integer positionCode;

}
