package ec.com.smx.an.service;

import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.vo.WorkAreaVo;
import java.util.List;

/**
 * Work Area services specification.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
public interface IWorkAreaService extends IBaseService<WorkAreaEntity> {

    /**
     * Find all work Area list.
     *
     * @return An array of Work Area's
     */
    List<WorkAreaVo> list();

    /**
     * Find work Area and positions list.
     *
     * @return An array of Work Area's
     */
    WorkAreaVo workAreaPositionsList(Integer workAreaCode);

    /**
     * Find work Area and positions list.
     *
     * @return An array of Work Area's
     */
    WorkAreaVo fidByCode(Integer workAreaCode);

}
