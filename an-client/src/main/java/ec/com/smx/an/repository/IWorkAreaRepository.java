package ec.com.smx.an.repository;

import ec.com.kruger.spring.orm.repository.IQueryDslBaseRepository;
import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.vo.WorkAreaVo;
import java.util.List;

/**
 * Position repository specification.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
public interface IWorkAreaRepository extends IQueryDslBaseRepository<WorkAreaEntity> {

    /**
     * Find all Work Area list.
     *
     * @return An array of Work Area's
     */
    List<WorkAreaVo> list();

    /**
     * Find Work Area by code.
     *
     * @return An array of Work Area's
     */
    WorkAreaVo fidByCode(Integer workAreaCode);

}
