package ec.com.smx.an.entity;

import lombok.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * position entity.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SOTTCARGO")
public class PositionEntity {

    @Id
    @Column(name = "CODIGOCARGO")
    private Integer positionCode;

    @Column(name = "CODIGOCOMPANIA")
    private Integer companyId;

    @Column(name = "NOMBRE")
    private String positionName;

    @Column(name = "ESTADO")
    private String status;

    @Column(name = "CODIGOEXTERNO")
    private Integer externalCode;

    @Column(name = "GRUPOCARGO")
    private String group;
}
