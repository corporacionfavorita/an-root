package ec.com.smx.an.entity;

import lombok.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Person entity.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTPERSONA")
public class PersonEntity {

    @Id
    @Column(name = "CODIGOPERSONA")
    private Integer personCode;

    @Column(name = "PRIMERNOMBRE")
    private String firstName;

    @Column(name = "SEGUNDONOMBRE")
    private String secondName;

    @Column(name = "PRIMERAPELLIDO")
    private String firstLastname;

    @Column(name = "SEGUNDOAPELLIDO")
    private String secondLastName;

    @Column(name = "NOMBRECOMPLETO")
    private String completeName;

}
