package ec.com.smx.an.entity;

import lombok.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Functionary entity.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SSPCOTFUNCIONARIO")
public class OfficialEntity {

    @Id
    @Column(name = "CODIGOFUNCIONARIO")
    private String officialCode;

    @Column(name = "CODIGOCOMPANIA")
    private Integer companyCode;

    @Column(name = "ESTADOFUNCIONARIO")
    private String status;

    @Column(name = "CODIGOPERSONA")
    private Integer personCode;

    @Column(name = "CODIGOAREATRABAJO")
    private Integer workAreaCode;

}
