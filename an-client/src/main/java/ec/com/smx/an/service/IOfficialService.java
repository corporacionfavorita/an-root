package ec.com.smx.an.service;

import ec.com.kruger.spring.service.IBaseService;
import ec.com.smx.an.entity.PersonEntity;
import ec.com.smx.an.vo.OfficialVo;
import java.util.List;

/**
 * Position services specification.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
public interface IOfficialService extends IBaseService<PersonEntity> {

    /**
     * Find person list by work area and position.
     *
     * @return An array of positions
     */
    List<OfficialVo> listByWorkAreaAndPosition(Integer workAreaCode, Integer positionCode);

    /**
     * Find person list by work area and position.
     *
     * @return An array of positions
     */
    OfficialVo findPersonByOfficialCode(String officialCode);

}
