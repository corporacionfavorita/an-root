package ec.com.smx.an.entity;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test OfficialEntity.
 * @author rzambrano on 2022/01/28.
 */

public class OfficialEntityTest {
    @Test
    public void testAssertEntityProperties() {
        OfficialEntity officialEntity = OfficialEntity.builder()
                .workAreaCode(123)
                .officialCode("1")
                .status("ACTIVO")
                .build();

        Assert.assertEquals(officialEntity.getStatus(), "ACTIVO");
        Assert.assertEquals("1", officialEntity.getOfficialCode());
    }
}
