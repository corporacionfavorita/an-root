//package ec.com.smx.an.entity;
//
//import com.github.javafaker.Faker;
//import org.junit.Assert;
//import org.junit.Test;
//
//public class PersonEntityTest {
//    private Faker faker = new Faker();
//
//    @Test
//    public void testGetId() {
//        PersonEntity personEntity = PersonEntity.builder()
//                .documentNumber("1752387504")
//                .email("test@krugercorp.ec")
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .build();
//        Assert.assertEquals(personEntity.getPersonId(), personEntity.getId());
//    }
//}
