package ec.com.smx.an.entity;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test PositionEntity
 * @author rzambrano on 2022/01/28.
 */

public class PositionEntityTest {
    @Test
    public void testAssertEntityProperties() {
        PositionEntity positionEntity = PositionEntity.builder()
                .companyId(123)
                .positionName("PROGRAMADOR")
                .build();

        Assert.assertEquals(positionEntity.getCompanyId(), positionEntity.getCompanyId());
        Assert.assertEquals(positionEntity.getPositionName(), "PROGRAMADOR");
    }
}
