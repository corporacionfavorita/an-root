package ec.com.smx.an.entity;

import com.github.javafaker.Faker;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test WorkAreaEntity.
 * @author rzambrano on 2022/01/28.
 */

public class WorkAreaEntityTest {
    private Faker faker = new Faker();

    @Test
    public void testAssertEntityProperties() {
        WorkAreaEntity workAreaEntity = WorkAreaEntity.builder()
                .workAreaCode(123)
                .companyCode(321)
                .name("OPERACIONES")
                .build();

        Assert.assertEquals(workAreaEntity.getWorkAreaCode(), workAreaEntity.getWorkAreaCode());
        Assert.assertEquals(workAreaEntity.getName(), "OPERACIONES");
    }
}
