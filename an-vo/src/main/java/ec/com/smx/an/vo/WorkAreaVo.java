package ec.com.smx.an.vo;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.EntityModel;

/**
 * Class VO for Work Area process.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkAreaVo extends EntityModel {
    private Integer workAreaCode;
    private Integer referenceCode;
    private String name;
    private String typeWorkAreaValue;
    private List<PositionVo> positions;
    private PositionVo position;
}
