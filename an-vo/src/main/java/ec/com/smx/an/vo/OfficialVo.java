package ec.com.smx.an.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Class VO for person process.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OfficialVo {
    private String officialCode;
    private Integer employeeReferenceCode;
    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    private String completeName;
    private Integer workAreaCode;
    private Integer positionCode;
    private WorkAreaVo workArea;
}
