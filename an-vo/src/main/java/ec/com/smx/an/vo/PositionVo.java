package ec.com.smx.an.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.EntityModel;
import java.util.List;

/**
 * Class VO for position process.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PositionVo extends EntityModel {
    private Integer positionCode;
    private Integer externalCode;
    private String positionName;
    private String status;
    private List<OfficialVo> persons;

}
