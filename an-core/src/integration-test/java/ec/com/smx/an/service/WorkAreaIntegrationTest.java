package ec.com.smx.an.service;

import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.vo.WorkAreaVo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * WorkArea Integration Test.
 *
 * @author rzambrano on 2022/02/01.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class WorkAreaIntegrationTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IWorkAreaService service;

    WorkAreaEntity workAreaEntity1;
    WorkAreaEntity workAreaEntity2;

    /**
     * For Integration tests, is a must to fill all the entity fields to persist.
     */
    @Before
    public void setup() {
        workAreaEntity1 = entityManager.persist(
                WorkAreaEntity.builder()
                        .workAreaCode(1)
                        .companyCode(1)
                        .referenceCode(1)
                        .status("ACT")
                        .name("OPERACIONES")
                        .build()
        );

        workAreaEntity2 = entityManager.persist(
                WorkAreaEntity.builder()
                        .workAreaCode(2)
                        .companyCode(2)
                        .referenceCode(2)
                        .status("ACT")
                        .name("FINANCIERO")
                        .build()
        );
    }

    @Test
    public void testList() {
        List<WorkAreaVo> workAreaVos = this.service.list();

        Assert.assertEquals(2, workAreaVos.size());
    }

    @Test
    public void testWorkAreaPositionsList() {
        WorkAreaVo workAreaVo = this.service.workAreaPositionsList(1);

        Assert.assertEquals(this.workAreaEntity1.getName(), workAreaVo.getName());
    }

    @Test
    public void testFidByCode() {
        WorkAreaVo workAreaVo = this.service.fidByCode(1);

        Assert.assertEquals(this.workAreaEntity1.getName(), workAreaVo.getName());
    }
}
