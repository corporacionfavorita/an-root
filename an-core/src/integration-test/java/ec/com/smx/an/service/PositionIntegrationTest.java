package ec.com.smx.an.service;

import ec.com.smx.an.vo.PositionVo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Position Integration Test.
 *
 * @author rzambrano on 2022/02/01.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class PositionIntegrationTest {
    @Autowired
    private IPositionService service;

    @Test
    public void testList() {
        List<PositionVo> list = this.service.list();

        Assert.assertNotNull(list);
    }

    @Test
    public void testListByWorkArea() {
        List<PositionVo> list = this.service.listByWorkArea(1);

        Assert.assertNotNull(list);
    }
}
