//package ec.com.smx.an.service;
//
//import com.github.javafaker.Faker;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//public class PersonIntegrationTest {
//
//    private Faker faker = new Faker();
//
//    @Autowired
//    private TestEntityManager entityManager;
//    @Autowired
//    private IOfficialService personService;
//
//    PersonEntity person1;
//    PersonEntity person2;
//
//    @Before
//    public void setup() {
//        person1=entityManager.persist(PersonEntity.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build());
//        person2=entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//    }
//    @Test
//    public void testFindPersonList() {
//        List<PersonVo> personEntityList = personService.list();
//        Assert.assertEquals(2, personEntityList.size());
//    }
//
//    @Test
//    public void testFindPersonById() {
//        PersonVo personVo = personService.findById(person1.getId());
//        Assert.assertEquals(person1.getPersonId(),personVo.getPersonId());
//    }
//}
