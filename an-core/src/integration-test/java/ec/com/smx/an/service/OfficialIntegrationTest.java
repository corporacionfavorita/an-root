package ec.com.smx.an.service;

import ec.com.smx.an.vo.OfficialVo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Official Integration Test.
 *
 * @author rzambrano on 2022/02/01.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfficialIntegrationTest {
    @Autowired
    private IOfficialService service;

    @Test
    public void testListByWorkAreaAndPosition() {
        List<OfficialVo> officialVos = this.service.listByWorkAreaAndPosition(1, 1);

        Assert.assertNotNull(officialVos);
    }
}
