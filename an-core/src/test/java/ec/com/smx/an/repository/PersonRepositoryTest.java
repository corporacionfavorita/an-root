//package ec.com.smx.an.repository;
//
//import com.github.javafaker.Faker;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@DataJpaTest
//public class PersonRepositoryTest {
//    private Faker faker = new Faker();
//
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @Autowired
//    private IOfficialRepository repository;
//
//    PersonEntity person1;
//    PersonEntity person2;
//
//    @Before
//    public void setup() {
//        person1 = entityManager.persist(PersonEntity.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build());
//        person2 = entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//        entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//        entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//        entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//        entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//        entityManager.persist(PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build());
//    }
//
//    @Test
//    public void testFindPersonList() {
//        List<PersonVo> personEntityList = repository.list();
//        Assert.assertEquals(7, personEntityList.size());
//    }
//
//    @Test
//    public void testFindPersonById() {
//        PersonEntity personEntity = repository.findById(person1.getId());
//        Assert.assertEquals(person1.getPersonId(), personEntity.getId());
//    }
//
//    @Test
//    public void testSavePerson() {
//        PersonEntity person3 = PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        repository.save(person3);
//        PersonEntity personEntity = repository.findById(person3.getId());
//        Assert.assertEquals(person3.getId(), personEntity.getId());
//    }
//
//    @Test
//    public void testUpdatePerson() {
//        person1.setFirstName("SERGIO");
//        repository.update(person1);
//        PersonEntity personEntity = repository.findById(person1.getId());
//        Assert.assertEquals("SERGIO", personEntity.getFirstName());
//    }
//    @Test
//    public void searchCriteriaTest() {
//        List<PersonVo> personEntityList = repository.listByCriteria(new PersonVo());
//        Assert.assertEquals(7, personEntityList.size());
//    }
//    @Test
//    public void searchCriteriaTest2() {
//        List<PersonVo> personEntityList = repository.listByCriteria(
//                PersonVo.builder()
//                        .firstName("Santiago")
//                        .lastName("Cuascota")
//                        .documentNumber("1752387504")
//                        .build());
//        Assert.assertEquals(1, personEntityList.size());
//    }
//    @Test
//    public void searchCriteriaTest3() {
//        List<PersonVo> personEntityList = repository.listByCriteria(
//                PersonVo.builder()
//                        .email("@.ec.com")
//                        .build());
//        Assert.assertEquals(0, personEntityList.size());
//    }
//
//    @Test
//    public void SearchPaginationTest() {
//        int page=3;
//        int size=3;
//        List<PersonVo> personEntityList = repository.listByCriteriaPaginated(new PersonVo(),page,size);
//        Assert.assertEquals(1, personEntityList.size());
//    }
//
//    @Test
//    public void getTotalRecordsTest() {
//        Long size = repository.getTotalRecords(new PersonVo());
//        Assert.assertEquals(7, size.longValue());
//    }
//}
