package ec.com.smx.an.repository;

import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.vo.WorkAreaVo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

/**
 * Test WorkAreaRepository.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class WorkAreaRepositoryTest {
    @Autowired
    private IWorkAreaRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void testList() {
        List<WorkAreaVo> workAreaVoList = this.repository.list();

        Assert.assertNotNull(workAreaVoList);
    }

    @Test
    public void testFidByCode() {
        entityManager.persist(
                WorkAreaEntity.builder()
                        .name("OPERACIONES")
                        .workAreaCode(123)
                        .build()
        );

        WorkAreaVo workAreaVo = this.repository.fidByCode(123);

        Assert.assertNotNull(workAreaVo);
        Assert.assertEquals("OPERACIONES", workAreaVo.getName());
    }
}
