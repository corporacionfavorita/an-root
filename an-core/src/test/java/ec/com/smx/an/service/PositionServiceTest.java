package ec.com.smx.an.service;

import static org.mockito.Mockito.when;

import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.repository.IPositionRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Test PositionService.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(MockitoJUnitRunner.class)
public class PositionServiceTest {
    @InjectMocks
    private PositionService positionService;

    @Mock
    private IPositionRepository positionRepository;

    @Test
    public void testList() {
        PositionVo positionVo = PositionVo.builder()
                .positionName("PROGRAMADOR")
                .positionCode(123)
                .build();

        List<PositionVo> positionVoList = new ArrayList<>();
        positionVoList.add(positionVo);

        when(this.positionRepository.list())
                .thenReturn(positionVoList);

        Assert.assertEquals(1, this.positionService.list().size());
    }

    @Test
    public void testListByWorkArea() {
        PositionVo positionVo = PositionVo.builder()
                .positionName("PROGRAMADOR")
                .positionCode(123)
                .build();

        List<PositionVo> positionVoList = new ArrayList<>();
        positionVoList.add(positionVo);

        when(this.positionRepository.listByWorkArea(1))
                .thenReturn(positionVoList);

        Assert.assertEquals(1, this.positionService.listByWorkArea(1).size());
    }

    @Test
    public void testFidByCode() {
        PositionVo mockPositionVo = PositionVo.builder()
                .positionCode(123)
                .positionName("PROGRAMADOR")
                .build();

        when(this.positionRepository.fidByCode(123))
                .thenReturn(mockPositionVo);

        PositionVo positionVo = this.positionService.fidByCode(123);

        Assert.assertNotNull(positionVo);
        Assert.assertEquals(positionVo.getPositionName(), "PROGRAMADOR");
    }
}
