package ec.com.smx.an.repository;

import ec.com.smx.an.entity.OfficialHistoryEntity;
import ec.com.smx.an.entity.PersonEntity;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.vo.OfficialVo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import ec.com.smx.an.entity.OfficialEntity;

import java.util.List;

/**
 * Test OfficialRepository.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfficialRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IOfficialRepository repository;

    PersonEntity personEntity;
    OfficialEntity officialEntity;
    OfficialHistoryEntity officialHistoryEntity;
    PositionEntity positionEntity;

    @Before
    public void setup() {
        personEntity = entityManager.persist(
                PersonEntity.builder()
                        .firstName("Regynald")
                        .secondName("Leonardo")
                        .firstLastname("Zambrano")
                        .secondLastName("Perero")
                        .personCode(1)
                        .build()
        );

        officialEntity = entityManager.persist(
                OfficialEntity.builder()
                        .personCode(1)
                        .officialCode("1")
                        .workAreaCode(1)
                        .status("ACT")
                        .build()
        );

        officialHistoryEntity = entityManager.persist(
                OfficialHistoryEntity.builder()
                        .employeeSequenceCode(1)
                        .officialCode("1")
                        .positionCode(1)
                        .endDate(null)
                        .build()
        );

        positionEntity = entityManager.persist(
                PositionEntity.builder()
                        .positionName("PROGRAMADOR")
                        .positionCode(1)
                        .build()
        );
    }

    @Test
    public void testListByWorkAreaAndPosition() {
        List<OfficialVo> officialVos = this.repository.listByWorkAreaAndPosition(1, 1);

        Assert.assertNotNull(officialVos);
    }

    @Test
    public void testFindPersonByOfficialCode() {
        OfficialVo officialVo = this.repository.findPersonByOfficialCode("1");

        Assert.assertNotNull(officialVo);
        Assert.assertEquals("Regynald", officialVo.getFirstName());
    }
}
