package ec.com.smx.an.service;

import ec.com.smx.an.repository.IPositionRepository;
import ec.com.smx.an.repository.IWorkAreaRepository;
import ec.com.smx.an.vo.WorkAreaVo;
import static org.mockito.Mockito.when;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Test WorkAreaService.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class WorkAreaServiceTest {
    @InjectMocks
    private WorkAreaService workAreaService;
    @InjectMocks
    private  PositionService positionService;

    @Mock
    private IWorkAreaRepository workAreaRepository;
    @Mock
    private IPositionRepository positionRepository;

    @Test
    public void testList() {
        WorkAreaVo object = new WorkAreaVo();
        object.setName("Prueba");

        List<WorkAreaVo> mock = new ArrayList<>();
        mock.add(object);

        when(this.workAreaRepository.list())
                .thenReturn(mock);

        List<WorkAreaVo> list = this.workAreaService.list();

        Assert.assertEquals(1, this.workAreaService.list().size());
    }

    @Test
    public void testFidByCode() {
        WorkAreaVo mockWorkAreaVo = WorkAreaVo.builder()
                .name("OPERACIONES")
                .build();

        when(this.workAreaRepository.fidByCode(123)).thenReturn(mockWorkAreaVo);

        WorkAreaVo workAreaVo = this.workAreaService.fidByCode(123);

        Assert.assertNotNull(workAreaVo);

        Assert.assertEquals(workAreaVo.getName(), "OPERACIONES");
    }
}
