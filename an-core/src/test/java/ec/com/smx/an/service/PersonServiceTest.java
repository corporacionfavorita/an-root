//package ec.com.smx.an.service;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//import ec.com.smx.an.vo.persons.PersonsPageResponse;
//import com.github.javafaker.Faker;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RunWith(MockitoJUnitRunner.class)
//public class PersonServiceTest {
//
//    private Faker faker = new Faker();
//
//    @InjectMocks
//    private OfficialService service;
//
//    @Mock
//    private IPersonRepository repository;
//
//
//    @Test
//    public void testFindList() {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.repository.list()).thenReturn(personVoList);
//        List<PersonVo> persons = this.service.list();
//        Assert.assertEquals(2, persons.size());
//    }
//
//    @Test
//    public void testFindById() {
//        PersonEntity personEntity=PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        when(this.repository.findById(any(String.class))).thenReturn(personEntity);
//        PersonVo personVo = this.service.findById("id");
//        Assert.assertEquals(personEntity.getFirstName(),personVo.getFirstName());
//    }
//
//    @Test(expected = EntityNotFoundException.class)
//    public void testFindByIdFail() {
//        when(this.repository.findById(any(String.class))).thenReturn(null);
//        PersonVo personVo = this.service.findById("id");
//    }
//
//    @Test
//    public void testSave() {
//        PersonVo person=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        PersonVo personVo = this.service.save(person);
//        Assert.assertEquals(person.getFirstName(),personVo.getFirstName());
//    }
//
//    @Test
//    public void testUpdate() {
//        PersonVo person=PersonVo.builder()
//                .personId("id")
//                .firstName("SANTY")
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        PersonEntity personEntity=PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(person.getLastName())
//                .documentNumber(person.getDocumentNumber())
//                .build();
//        when(this.repository.findById(any(String.class))).thenReturn(personEntity);
//        PersonVo personVo = this.service.update(person);
//        Assert.assertEquals(person.getFirstName(),personVo.getFirstName());
//    }
//
//    @Test(expected = EntityNotFoundException.class)
//    public void testUpdateFail() {
//        PersonVo person=PersonVo.builder()
//                .personId("id")
//                .firstName("SANTY")
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        when(this.repository.findById(any(String.class))).thenReturn(null);
//        this.service.update(person);
//    }
//
//    @Test
//    public void testDelete() {
//        PersonEntity personEntity=PersonEntity.builder()
//                .firstName("SANTY")
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        when(this.repository.findById(any(String.class))).thenReturn(personEntity);
//        this.service.delete("id");
//        Assert.assertEquals(Boolean.FALSE,personEntity.getStatus());
//    }
//
//    @Test
//    public void testGetListByCriteria() {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.repository.listByCriteria(any(PersonVo.class))).thenReturn(personVoList);
//
//        Map<String,String> queryParams=new HashMap<>();
//        queryParams.put("documentNumber","");
//        queryParams.put("email","");
//        queryParams.put("firstName","");
//        queryParams.put("lastName","");
//
//        List<PersonVo> personVoResponse=this.service.listByCriteria(queryParams);
//        Assert.assertEquals(2,personVoResponse.size());
//    }
//
//    @Test
//    public void testListByCriteriaPaginated() {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.repository.listByCriteriaPaginated(any(PersonVo.class), any(Integer.class), any(Integer.class)))
//                .thenReturn(personVoList);
//        when(this.repository.getTotalRecords(any(PersonVo.class)))
//                .thenReturn(new Long(2));
//
//        Map<String,String> queryParams=new HashMap<>();
//        queryParams.put("documentNumber","");
//        queryParams.put("email","");
//        queryParams.put("firstName","");
//        queryParams.put("lastName","");
//        queryParams.put("page","1");
//        queryParams.put("size","2");
//
//        PersonsPageResponse personsPageResponse=this.service.listByCriteriaPaginated(queryParams);
//        Assert.assertNotNull(personsPageResponse);
//    }
//
//    @Test
//    public void testListByCriteriaPaginated2() {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.repository.listByCriteriaPaginated(any(PersonVo.class), any(Integer.class), any(Integer.class)))
//                .thenReturn(personVoList);
//        Map<String,String> queryParams=new HashMap<>();
//        queryParams.put("documentNumber","");
//        queryParams.put("email","");
//        queryParams.put("firstName","");
//        queryParams.put("lastName","");
//        queryParams.put("page","2");
//        queryParams.put("size","2");
//
//        PersonsPageResponse personsPageResponse=this.service.listByCriteriaPaginated(queryParams);
//        Assert.assertNotNull(personsPageResponse);
//    }
//
//
//}
