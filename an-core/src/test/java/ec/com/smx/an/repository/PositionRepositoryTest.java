package ec.com.smx.an.repository;

import ec.com.smx.an.entity.OfficialEntity;
import ec.com.smx.an.entity.OfficialHistoryEntity;
import ec.com.smx.an.entity.PersonEntity;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.vo.PositionVo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Test PositionRepository.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class PositionRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IPositionRepository positionRepository;

    OfficialEntity officialEntity;
    OfficialHistoryEntity officialHistoryEntity;
    PositionEntity positionEntity;
    PersonEntity personEntity;

    @Before
    public void setup() {
        personEntity = entityManager.persist(
                PersonEntity.builder()
                        .firstName("Regynald")
                        .secondName("Leonardo")
                        .firstLastname("Zambrano")
                        .secondLastName("Perero")
                        .personCode(1)
                        .build()
        );

        officialEntity = entityManager.persist(
                OfficialEntity.builder()
                        .personCode(1)
                        .officialCode("1")
                        .workAreaCode(1)
                        .status("ACT")
                        .build()
        );

        officialHistoryEntity = entityManager.persist(
                OfficialHistoryEntity.builder()
                        .employeeSequenceCode(1)
                        .officialCode("1")
                        .positionCode(1)
                        .endDate(null)
                        .build()
        );

        positionEntity = entityManager.persist(
                PositionEntity.builder()
                        .positionName("PROGRAMADOR")
                        .positionCode(1)
                        .build()
        );
    }

    @Test
    public void testList() {
        List<PositionVo> positionVoList = this.positionRepository.list();

        Assert.assertNotNull(positionVoList);
    }

    @Test
    public void testListByWorkArea() {
        List<PositionVo> positionVoList = this.positionRepository.listByWorkArea(1);

        Assert.assertNotNull(positionVoList);
        Assert.assertEquals("PROGRAMADOR", positionVoList.get(0).getPositionName());
    }

    @Test
    public void testFidByCode() {
        PositionVo positionVo = this.positionRepository.fidByCode(1);

        Assert.assertNotNull(positionVo);
        Assert.assertEquals("PROGRAMADOR", positionVo.getPositionName());
    }
}
