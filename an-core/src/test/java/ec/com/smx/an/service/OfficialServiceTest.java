package ec.com.smx.an.service;

import static org.mockito.Mockito.when;

import ec.com.smx.an.repository.IOfficialRepository;
import ec.com.smx.an.vo.OfficialVo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Test OfficialService.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(MockitoJUnitRunner.class)
public class OfficialServiceTest {
    @InjectMocks
    private OfficialService officialService;

    @Mock
    private IOfficialRepository iOfficialRepository;

    @Test
    public void testListByWorkAreaAndPosition() {
        OfficialVo mockOfficialVo = OfficialVo.builder()
                .workAreaCode(123)
                .positionCode(1)
                .build();

        List<OfficialVo> mockOfficialVos = new ArrayList<>();
        mockOfficialVos.add(mockOfficialVo);

        when(this.iOfficialRepository.listByWorkAreaAndPosition(123, 1))
                .thenReturn(mockOfficialVos);

        Assert.assertNotNull(this.officialService.listByWorkAreaAndPosition(123, 1));
    }
}
