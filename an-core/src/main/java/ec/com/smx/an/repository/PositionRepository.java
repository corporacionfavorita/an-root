package ec.com.smx.an.repository;

import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.vo.PositionVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import java.util.List;
import static com.querydsl.core.types.Projections.bean;
import static ec.com.smx.an.entity.QPositionEntity.positionEntity;
import static ec.com.smx.an.entity.QOfficialHistoryEntity.officialHistoryEntity;
import static ec.com.smx.an.entity.QOfficialEntity.officialEntity;

/**
 * Position repository compile with JPA and QUERYDSL.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Repository
public class PositionRepository extends JPAQueryDslBaseRepository<PositionEntity> implements IPositionRepository{

    /**
     * Constructor.
     */
    public PositionRepository() {
        super(PositionEntity.class);
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public List<PositionVo> list() {
        return from(positionEntity)
                .select(bean(PositionVo.class,
                        positionEntity.externalCode,
                        positionEntity.positionName,
                        positionEntity.status))
                .where(positionEntity.group.eq("GTH"))
                .fetch();
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public List<PositionVo> listByWorkArea(Integer workAreaCode) {
        return from(positionEntity)
                .select(bean(PositionVo.class,
                        positionEntity.positionCode,
                        positionEntity.externalCode,
                        positionEntity.positionName,
                        positionEntity.status))
                .distinct()
                .innerJoin(officialHistoryEntity)
                .on(positionEntity.positionCode.eq(officialHistoryEntity.positionCode)
                        .and(officialHistoryEntity.endDate.isNull()))
                .innerJoin(officialEntity)
                .on(officialHistoryEntity.officialCode.eq(officialEntity.officialCode))
                .where(officialEntity.workAreaCode.eq(workAreaCode)
                        .and(officialEntity.status.eq("ACT")))
                .fetch();
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public PositionVo fidByCode(Integer positionCode) {
        return from(positionEntity)
                .select(bean(PositionVo.class,
                        positionEntity.positionCode,
                        positionEntity.externalCode,
                        positionEntity.positionName,
                        positionEntity.status))
                .where(positionEntity.positionCode.eq(positionCode))
                .fetchOne();
    }


}
