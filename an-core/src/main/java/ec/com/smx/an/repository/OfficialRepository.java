package ec.com.smx.an.repository;

import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.an.entity.PersonEntity;
import ec.com.smx.an.vo.OfficialVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import java.util.List;
import static com.querydsl.core.types.Projections.bean;
import static ec.com.smx.an.entity.QOfficialEntity.officialEntity;
import static ec.com.smx.an.entity.QOfficialHistoryEntity.officialHistoryEntity;
import static ec.com.smx.an.entity.QPositionEntity.positionEntity;
import static ec.com.smx.an.entity.QPersonEntity.personEntity;

/**
 * Position repository compile with JPA and QUERYDSL.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Repository
public class OfficialRepository extends JPAQueryDslBaseRepository<PersonEntity> implements IOfficialRepository {

    /**
     * Constructor.
     */
    public OfficialRepository() {
        super(PersonEntity.class);
    }

    @Override
    public List<OfficialVo> listByWorkAreaAndPosition(Integer workAreaCode, Integer positionCode) {
        return from(personEntity)
                .select(bean(OfficialVo.class,
                        officialHistoryEntity.officialCode,
                        officialHistoryEntity.employeeReferenceCode,
                        personEntity.firstName,
                        personEntity.secondName,
                        personEntity.firstLastname,
                        personEntity.secondLastName,
                        personEntity.completeName))
                .innerJoin(officialEntity)
                .on(officialEntity.personCode.eq(personEntity.personCode))
                .innerJoin(officialHistoryEntity)
                .on(officialHistoryEntity.officialCode.eq(officialEntity.officialCode)
                        .and(officialHistoryEntity.endDate.isNull()))
                .innerJoin(positionEntity)
                .on(positionEntity.positionCode.eq(officialHistoryEntity.positionCode))
                .where(officialEntity.workAreaCode.eq(workAreaCode)
                        .and(officialEntity.status.eq("ACT"))
                        .and(positionEntity.positionCode.eq(positionCode)))
                .fetch();
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public OfficialVo findPersonByOfficialCode(String officialCode) {
        return from(personEntity)
                .select(bean(OfficialVo.class,
                        officialHistoryEntity.officialCode,
                        officialHistoryEntity.employeeReferenceCode,
                        personEntity.firstName,
                        personEntity.secondName,
                        personEntity.firstLastname,
                        personEntity.secondLastName,
                        personEntity.completeName,
                        officialEntity.workAreaCode,
                        positionEntity.positionCode))
                .innerJoin(officialEntity)
                .on(officialEntity.personCode.eq(personEntity.personCode))
                .innerJoin(officialHistoryEntity)
                .on(officialHistoryEntity.officialCode.eq(officialEntity.officialCode)
                        .and(officialHistoryEntity.endDate.isNull()))
                .innerJoin(positionEntity)
                .on(positionEntity.positionCode.eq(officialHistoryEntity.positionCode))
                .where(officialEntity.officialCode.eq(officialCode)
                        .and(officialEntity.status.eq("ACT")))
                .fetchOne();
    }
}
