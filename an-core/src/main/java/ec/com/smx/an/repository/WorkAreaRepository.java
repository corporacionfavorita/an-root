package ec.com.smx.an.repository;

import static com.querydsl.core.types.Projections.bean;
import static ec.com.smx.an.entity.QWorkAreaEntity.workAreaEntity;

import java.util.List;
import ec.com.kruger.spring.orm.jpa.repository.JPAQueryDslBaseRepository;
import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.vo.WorkAreaVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

/**
 * Work Area repository compile with JPA and QUERYDSL.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Repository
public class WorkAreaRepository extends JPAQueryDslBaseRepository<WorkAreaEntity> implements
    IWorkAreaRepository {

    /**
     * Constructor.
     */
    public WorkAreaRepository() {
        super(WorkAreaEntity.class);
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public List<WorkAreaVo> list() {
        return from(workAreaEntity)
            .select(bean(WorkAreaVo.class,
                workAreaEntity.workAreaCode,
                workAreaEntity.referenceCode,
                workAreaEntity.name,
                workAreaEntity.typeWorkAreaValue))
            .where(workAreaEntity.status.eq("ACT"))
            .fetch();
    }

    /**
     * Find persons active list.
     *
     * @return An array of persons
     */
    @Override
    public WorkAreaVo fidByCode(Integer workAreaCode) {
        return from(workAreaEntity)
            .select(bean(WorkAreaVo.class,
                workAreaEntity.workAreaCode,
                workAreaEntity.referenceCode,
                workAreaEntity.name,
                workAreaEntity.typeWorkAreaValue))
            .where(workAreaEntity.workAreaCode.eq(workAreaCode))
            .fetchOne();
    }

}
