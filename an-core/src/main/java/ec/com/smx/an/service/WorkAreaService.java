package ec.com.smx.an.service;

import ec.com.kruger.spring.service.jpa.BaseService;
import ec.com.smx.an.entity.WorkAreaEntity;
import ec.com.smx.an.exception.AnRuntimeException;
import ec.com.smx.an.repository.IWorkAreaRepository;
import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.vo.WorkAreaVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Position service implementation.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Service
public class WorkAreaService extends BaseService<WorkAreaEntity, IWorkAreaRepository> implements
    IWorkAreaService {

    @Autowired
    @Lazy
    private IPositionService positionService;

    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public WorkAreaService(IWorkAreaRepository repository) {
        super(repository);
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    @Transactional(readOnly = true)
    public List<WorkAreaVo> list() {
        return this.repository.list();
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public WorkAreaVo workAreaPositionsList(Integer workAreaCode) {
        List<PositionVo> positions;
        WorkAreaVo workArea;
        positions =this.positionService.listByWorkArea(workAreaCode);
        workArea=this.repository.fidByCode(workAreaCode);
        if(workArea==null){
            throw new AnRuntimeException("No se ha encontrado el Area de Trabajo");
        }
        workArea.setPositions(positions);
        return workArea;
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public WorkAreaVo fidByCode(Integer workAreaCode) {
        return this.repository.fidByCode(workAreaCode);
    }


}
