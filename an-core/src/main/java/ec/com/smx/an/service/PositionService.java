package ec.com.smx.an.service;

import ec.com.kruger.spring.service.jpa.BaseService;
import ec.com.smx.an.entity.PositionEntity;
import ec.com.smx.an.exception.AnRuntimeException;
import ec.com.smx.an.repository.IPositionRepository;
import ec.com.smx.an.vo.OfficialVo;
import ec.com.smx.an.vo.PositionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Position service implementation.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Service
public class PositionService extends BaseService<PositionEntity, IPositionRepository> implements
    IPositionService {

    @Autowired
    @Lazy
    private IOfficialService personService;
    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public PositionService(IPositionRepository repository) {
        super(repository);
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    @Transactional(readOnly = true)
    public List<PositionVo> list() {
        return this.repository.list();
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public List<PositionVo> listByWorkArea(Integer workAreaCode) {
        return this.repository.listByWorkArea(workAreaCode);
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public PositionVo positionPersonList(Integer workAreaCode, Integer positionCode) {
        List<OfficialVo> persons;
        PositionVo position;
        persons = this.personService.listByWorkAreaAndPosition(workAreaCode, positionCode);
        position =this.repository.fidByCode(positionCode);
        if(position==null){
            throw new AnRuntimeException("No se ha encontrado el Cargo");
        }
        position.setPersons(persons);
        return position;
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public PositionVo fidByCode(Integer positionCode) {
        return this.repository.fidByCode(positionCode);
    }


}
