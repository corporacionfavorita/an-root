package ec.com.smx.an.config;

import javax.persistence.EntityManagerFactory;
import ec.com.kruger.spring.orm.jpa.config.SecurityAuditListenerConfig;
import ec.com.smx.frameworkv2.security.view.UserView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * An spring configuration.
 *
 * @author scuascota on 2021/08/02.
 * @version 1.0
 */
@EntityScan(basePackageClasses = UserView.class, basePackages = "ec.com.smx.an.entity")
@EnableJpaRepositories(basePackages = "ec.com.smx.an.repository")
@ComponentScan(basePackages = "ec.com.smx.an")
@EnableTransactionManagement
@Import(SecurityAuditListenerConfig.class)
public class AnConfiguration {

    /**
     * <p>
     * transactionManager.
     * </p>
     *
     * @param emf a {@link javax.persistence.EntityManagerFactory} object.
     * @return a {@link org.springframework.transaction.PlatformTransactionManager} object.
     */
    @Bean
    @Autowired
    public PlatformTransactionManager jpaTransactionManager(EntityManagerFactory emf) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(emf);
        return txManager;
    }

}
