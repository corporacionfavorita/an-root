package ec.com.smx.an.service;

import java.util.List;
import ec.com.kruger.spring.service.jpa.BaseService;
import ec.com.smx.an.entity.PersonEntity;
import ec.com.smx.an.exception.AnRuntimeException;
import ec.com.smx.an.repository.IOfficialRepository;
import ec.com.smx.an.vo.OfficialVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Position service implementation.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@Lazy
@Service
public class OfficialService extends BaseService<PersonEntity, IOfficialRepository> implements
    IOfficialService {

    @Autowired
    @Lazy
    private IPositionService positionService;

    @Autowired
    @Lazy
    private IWorkAreaService workAreaService;

    /**
     * Constructor with dependencies.
     *
     * @param repository The repository to inject
     */
    public OfficialService(IOfficialRepository repository) {
        super(repository);
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public List<OfficialVo> listByWorkAreaAndPosition(Integer workAreaCode, Integer positionCode) {
        return this.repository.listByWorkAreaAndPosition(workAreaCode, positionCode);
    }

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @Override
    public OfficialVo findPersonByOfficialCode(String officialCode) {
        OfficialVo officialVo = this.repository.findPersonByOfficialCode(officialCode);
        if (officialVo == null) {
            throw new AnRuntimeException("No se ha encontrado el Funcionario");
        }
        officialVo.setWorkArea(this.workAreaService.fidByCode(officialVo.getWorkAreaCode()));
        officialVo.getWorkArea()
            .setPosition(this.positionService.fidByCode(officialVo.getPositionCode()));
        return officialVo;
    }
}
