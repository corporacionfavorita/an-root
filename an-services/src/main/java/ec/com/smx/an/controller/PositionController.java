package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.smx.an.service.IPositionService;
import ec.com.smx.an.vo.PositionVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Class to controller position rest services.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/positions")
@Lazy
@Slf4j
@Tag(name = "Positions", description = "The Positions API")
public class PositionController extends BaseController {

    @Lazy
    @Autowired
    private IPositionService service;

    /**
     * Find persons active list.
     *
     * @return An array of person
     */
    @GetMapping(path = "",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get list of positions")
    public ResponseEntity<List<PositionVo>> list() {
        return ResponseEntity.ok(this.service.list());
    }

    /**
     * Find work Area and position list.
     *
     * @return Work Area
     */
    @GetMapping(path = "/{workAreaCode}/{positionCode}/officials",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get position with list of officials by workAreaCode and positionCode")
    public ResponseEntity<PositionVo> positionPersonList(@PathVariable Integer workAreaCode,@PathVariable Integer positionCode) {
        return ResponseEntity.ok(this.service.positionPersonList(workAreaCode,positionCode));
    }
}
