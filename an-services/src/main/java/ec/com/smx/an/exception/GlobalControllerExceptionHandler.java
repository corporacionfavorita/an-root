package ec.com.smx.an.exception;

import com.fasterxml.jackson.core.JsonParseException;
import ec.com.kruger.spring.ws.exception.MessageError;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;

/**
 * Global Handler to generic or business exceptions.
 *
 * @author scuascota on 2021/08/05.
 * @version 1.0
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    /**
     * Json Parse exception.
     *
     * @param ex Exception exception.
     * @return ResponseEntity with specific code.
     */
    @ExceptionHandler(JsonParseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<MessageError> jsonParseException(HttpServletRequest request, Exception ex) {
        return buildResponseEntity(request, HttpStatus.BAD_REQUEST,ex);
    }

    /**
     * Null Pointer exception.
     *
     * @param ex Exception exception.
     * @return ResponseEntity with specific code.
     */
    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<MessageError> nullPointerException(HttpServletRequest request, Exception ex) {
        return buildResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR,ex);
    }


    /**
     * Constraint Violation exception.
     *
     * @param ex Exception exception.
     * @return ResponseEntity with specific code.
     */
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<MessageError> constraintViolationException(HttpServletRequest request, Exception ex) {
        return buildResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR,ex);
    }

    /**
     * Http Request Method Not Supported exception.
     *
     * @param ex Exception exception.
     * @return ResponseEntity with specific code.
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<MessageError> httpRequestMethodNotSupportedException(HttpServletRequest request, Exception ex) {
        return buildResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR,ex);
    }

    private ResponseEntity<MessageError> buildResponseEntity(HttpServletRequest request, HttpStatus status, Exception ex) {
        MessageError error=new MessageError(
                status.toString(),
                ex.getMessage(),
                request.getRequestURI()
        );
        return new ResponseEntity<>(error,status);
    }
}
