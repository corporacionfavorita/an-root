package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.smx.an.service.OfficialService;
import ec.com.smx.an.vo.OfficialVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class to controller position rest services.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/official")
@Lazy
@Slf4j
@Tag(name = "Official", description = "The Official API")
public class OfficialController extends BaseController {

    @Lazy
    @Autowired
    private OfficialService service;

    /**
     * Find work Area and position list.
     *
     * @return Work Area
     */
    @GetMapping(path = "/{officialCode}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get official information by OfficialCode")
    public ResponseEntity<OfficialVo> findPersonByOfficialCode(@PathVariable String officialCode) {
        return ResponseEntity.ok(this.service.findPersonByOfficialCode(officialCode));
    }
}
