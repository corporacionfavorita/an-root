package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.BaseController;
import ec.com.kruger.utilitario.loggin.util.AmbienteConstantes;
import ec.com.smx.an.common.AnConstants;
import ec.com.smx.an.service.IWorkAreaService;
import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.vo.WorkAreaVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * Class to controller Work Area rest services.
 *
 * @author scuascota on 2021/12/15.
 * @version 1.0
 */
@RestController
@RequestMapping("/api/v1/work_area")
@Lazy
@Slf4j
@Tag(name = "Work Area", description = "The Work Area API")
public class WorkAreaController extends BaseController {

    @Lazy
    @Autowired
    private IWorkAreaService service;

    /**
     * Find work Area list.
     *
     * @return An array of Work Area
     */
    @GetMapping(path = "",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get list of work Areas")
    public ResponseEntity<List<WorkAreaVo>> list() {
        List<WorkAreaVo> workAreaVoList=this.service.list();
        for (WorkAreaVo workArea: workAreaVoList) {
            addPositionsLink(workArea);
        }
        return ResponseEntity.ok(workAreaVoList);
    }

    /**
     * Find work Area and position list.
     *
     * @return Work Area
     */
    @GetMapping(path = "/{workAreaCode}/positions",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Get work Area with list of positions")
    public ResponseEntity<WorkAreaVo> workAreaPositionsList(@PathVariable Integer workAreaCode) {
        WorkAreaVo workArea=this.service.workAreaPositionsList(workAreaCode);
        for (PositionVo position:workArea.getPositions()) {
            addPersonListLink(position,workArea.getWorkAreaCode());
        }
        return ResponseEntity.ok(workArea);
    }

    private void addPositionsLink(WorkAreaVo workAreaVo) {
        workAreaVo.add(filterLink(
                linkTo(methodOn(WorkAreaController.class)
                        .workAreaPositionsList(workAreaVo.getWorkAreaCode()))
                        .withRel("Cargos")));
    }

    private void addPersonListLink(PositionVo positionVo, Integer workAreaCode) {
        positionVo.add(filterLink(
                linkTo(methodOn(PositionController.class)
                        .positionPersonList(workAreaCode,positionVo.getPositionCode()))
                        .withRel("Funcionarios")));
    }

    private Link filterLink(Link link){
        if(AmbienteConstantes.AMBIENTE_ACTUAL.equals(AnConstants.CONSOLE_ENV)){
            return link;
        }else{
            return link.withHref("https"+link.getHref().substring(4));
        }
    }

}
