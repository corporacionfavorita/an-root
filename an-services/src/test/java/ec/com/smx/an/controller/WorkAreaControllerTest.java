package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
import ec.com.smx.an.service.IWorkAreaService;
import ec.com.smx.an.vo.OfficialVo;
import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.vo.WorkAreaVo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test WorkAreaController.
 * @author rzambrano on 2022/01/28.
 */

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(WorkAreaController.class)
public class WorkAreaControllerTest extends MockMvcControllerBase {
    @InjectMocks
    private WorkAreaController workAreaController;

    @Mock
    private IWorkAreaService service;

    @Before
    public void setup() {
        super.setUpBase(this.workAreaController);
    }

    @Test
    public void testList() throws Exception {
        WorkAreaVo workAreaVo = WorkAreaVo.builder()
                .name("OPERACIONES")
                .workAreaCode(1)
                .build();

        List<WorkAreaVo> workAreaVoList = new ArrayList<>();
        workAreaVoList.add(workAreaVo);

        when(this.service.list())
                .thenReturn(workAreaVoList);

        mockMvc.perform(get("/anServices/api/v1/work_area").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testWorkAreaPositionsList() throws Exception {
        WorkAreaVo workAreaVo = WorkAreaVo.builder()
                .name("OPERACIONES")
                .workAreaCode(1)
                .build();

        OfficialVo officialVo = OfficialVo.builder()
                .workAreaCode(1)
                .positionCode(2)
                .workArea(workAreaVo)
                .build();

        List<OfficialVo> officialVos = new ArrayList<>();
        officialVos.add(officialVo);

        PositionVo positionVo = PositionVo.builder()
                .positionCode(2)
                .positionName("PROGRAMADOR")
                .persons(officialVos)
                .build();

        List<PositionVo> positionVoList = new ArrayList<>();
        positionVoList.add(positionVo);

        workAreaVo.setPositions(positionVoList);

        when(this.service.workAreaPositionsList(1))
                .thenReturn(workAreaVo);

        mockMvc.perform(get("/anServices/api/v1/work_area/1/positions").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
