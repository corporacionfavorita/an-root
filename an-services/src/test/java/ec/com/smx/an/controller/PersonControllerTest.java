//package ec.com.smx.an.controller;
//
//import com.github.javafaker.Faker;
//import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.CoreMatchers.is;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//
//
//@RunWith(MockitoJUnitRunner.class)
//@WebMvcTest(PersonController.class)
//public class PersonControllerTest  extends MockMvcControllerBase {
//
//    private Faker faker = new Faker();
//
//    @InjectMocks
//    private PersonController controller;
//
//    @Mock
//    private IPersonService service;
//
//    @Before
//    public void setup() {
//        super.setUpBase(controller);
//    }
//
//    @Test
//    public void testFindList() throws Exception {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.service.list()).thenReturn(personVoList);
//
//        mockMvc.perform(get("/anServices/api/v1/persons").contextPath("/anServices")
//                        .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].firstName", is(person1.getFirstName())))
//                .andExpect(jsonPath("$[1].firstName", is(person2.getFirstName())))
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).list();
//    }
//
//    @Test
//    public void testFindById() throws Exception {
//        PersonVo personEntity=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        when(this.service.findById(any(String.class))).thenReturn(personEntity);
//
//        mockMvc.perform(get("/anServices/api/v1/persons/id").contextPath("/anServices")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(personEntity.getFirstName())))
//                .andExpect(jsonPath("lastName", is(personEntity.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).findById("id");
//    }
//
//    @Test
//    public void testSave() throws Exception {
//        PersonVo person=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        when(this.service.save(any(PersonVo.class))).thenReturn(person);
//
//        mockMvc.perform(post("/anServices/api/v1/persons").contextPath("/anServices")
//                .content(objectMapper.writeValueAsString(person))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(person.getFirstName())))
//                .andExpect(jsonPath("lastName", is(person.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).save(person);
//    }
//
//    @Test
//    public void testUpdate() throws Exception {
//        PersonVo person=PersonVo.builder()
//                .firstName("SANTY")
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        when(this.service.update(any(PersonVo.class))).thenReturn(person);
//
//        mockMvc.perform(put("/anServices/api/v1/persons").contextPath("/anServices")
//                .content(objectMapper.writeValueAsString(person))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(person.getFirstName())))
//                .andExpect(jsonPath("lastName", is(person.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).update(person);
//    }
//
//    @Test
//    public void testDelete() throws Exception {
//
//        mockMvc.perform(delete("/anServices/api/v1/persons/id").contextPath("/anServices")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).delete("id");
//    }
//
//    @Test
//    public void testGetListByCriteria() throws Exception {
//        PersonVo person1=PersonVo.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        PersonVo person2=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        List<PersonVo> personVoList=new ArrayList<>();
//        personVoList.add(person1);
//        personVoList.add(person2);
//
//        when(this.service.listByCriteria(any(Map.class))).thenReturn(personVoList);
//
//        Map<String,String> queryParams=new HashMap<>();
//        queryParams.put("documentNumber","");
//        queryParams.put("email","");
//        queryParams.put("firstName","");
//        queryParams.put("lastName","");
//
//        mockMvc.perform(get("/anServices/api/v1/persons/search").contextPath("/anServices")
//                .param("documentNumber","")
//                .param("email","")
//                .param("firstName","")
//                .param("lastName","")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andDo(MockMvcResultHandlers.print());
//        verify(service, times(1)).listByCriteria(queryParams);
//    }
//
//
//}
