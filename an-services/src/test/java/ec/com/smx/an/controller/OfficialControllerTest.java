package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;

import ec.com.smx.an.service.IOfficialService;
import ec.com.smx.an.service.OfficialService;
import ec.com.smx.an.vo.OfficialVo;
import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.vo.WorkAreaVo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test OfficialController.
 * @author rzambrano on 2022/01/31.
 */

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(OfficialController.class)
public class OfficialControllerTest extends MockMvcControllerBase {
    @InjectMocks
    private OfficialController officialController;

    @Mock
    private OfficialService service;

    @Before
    public void setup() {
        super.setUpBase(this.officialController);
    }

    @Test
    public void testFindPersonByOfficialCode() throws Exception {
        OfficialVo officialVo = OfficialVo.builder()
                .officialCode("abc")
                .workAreaCode(1)
                .positionCode(1)
                .firstName("REGYNALD")
                .build();

        when(this.service.findPersonByOfficialCode(officialVo.getOfficialCode()))
                .thenReturn(officialVo);



        mockMvc.perform(get("/anServices/api/v1/official/abc").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
