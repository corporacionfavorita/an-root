package ec.com.smx.an.exception;

import ec.com.kruger.spring.ws.exception.MessageError;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

public class GlobalControllerExceptionHandlerTest {
    private GlobalControllerExceptionHandler globalControllerExceptionHandler;

    @Before
    public void setup() {
        this.globalControllerExceptionHandler = new GlobalControllerExceptionHandler();
    }

    @Test
    public void jsonParseExceptionTest(){
        ResponseEntity<MessageError> response = this.globalControllerExceptionHandler
                .jsonParseException(new MockHttpServletRequest(), new Exception());
        Assert.assertEquals( HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void nullPointerExceptionTest(){
        ResponseEntity<MessageError> response = this.globalControllerExceptionHandler
                .nullPointerException(new MockHttpServletRequest(), new Exception());
        Assert.assertEquals( HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void constraintViolationExceptionTest(){
        ResponseEntity<MessageError> response = this.globalControllerExceptionHandler
                .constraintViolationException(new MockHttpServletRequest(), new Exception());
        Assert.assertEquals( HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    public void httpRequestMethodNotSupportedExceptionTest(){
        ResponseEntity<MessageError> response = this.globalControllerExceptionHandler
                .httpRequestMethodNotSupportedException(new MockHttpServletRequest(), new Exception());
        Assert.assertEquals( HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}
