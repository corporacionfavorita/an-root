package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;

import ec.com.smx.an.service.IPositionService;
import ec.com.smx.an.vo.OfficialVo;
import ec.com.smx.an.vo.PositionVo;
import ec.com.smx.an.vo.WorkAreaVo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test PositionController.
 * @author rzambrano on 2022/01/31.
 */

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(PositionController.class)
public class PositionControllerTest extends MockMvcControllerBase {
    @InjectMocks
    private PositionController positionController;

    @Mock
    private IPositionService service;

    @Before
    public void setup() {
        super.setUpBase(this.positionController);
    }

    @Test
    public void testList() throws Exception {
        PositionVo positionVo = PositionVo.builder()
                .positionCode(1)
                .positionName("PROGRAMADOR")
                .build();

        List<PositionVo> positionVoList = new ArrayList<>();
        positionVoList.add(positionVo);

        when(this.service.list())
                .thenReturn(positionVoList);

        mockMvc.perform(get("/anServices/api/v1/positions").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    public void testPositionPersonList() throws Exception {
        PositionVo positionVo = PositionVo.builder()
                .positionCode(1)
                .positionName("PROGRAMADOR")
                .build();

        when(this.service.positionPersonList(1, 1))
                .thenReturn(positionVo);

        mockMvc.perform(get("/anServices/api/v1/positions/1/1/officials").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
