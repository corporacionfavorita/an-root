package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
import ec.com.smx.an.TestConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * WorkArea Integration Test.
 *
 * @author rzambrano on 2022/02/01.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application.yaml")
@Import(TestConfig.class)
public class PositionIntegrationTest extends MockMvcControllerBase {
    @Autowired
    private PositionController controller;

    @Before
    public void setup() {
        super.setUpBase(this.controller);
    }

    @Test
    public void testList() throws Exception {
        mockMvc.perform(get("/anServices/api/v1/positions").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
