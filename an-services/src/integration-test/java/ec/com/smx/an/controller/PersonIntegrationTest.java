//package ec.com.smx.an.controller;
//
//import com.github.javafaker.Faker;
//import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
//import ec.com.smx.an.TestConfig;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.context.annotation.Import;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
//import static org.hamcrest.CoreMatchers.is;
//import static org.hamcrest.Matchers.hasSize;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//
//@RunWith(SpringRunner.class)
//@TestPropertySource("classpath:application.yaml")
//@Import(TestConfig.class)
//@DataJpaTest
//public class PersonIntegrationTest extends MockMvcControllerBase {
//
//    private Faker faker = new Faker();
//
//    @Autowired
//    private TestEntityManager entityManager;
//    @Autowired
//    private PersonController controller;
//
//    PersonEntity person1;
//    PersonEntity person2;
//
//    @Before
//    public void setup() {
//        super.setUpBase(controller);
//        person1=PersonEntity.builder()
//                .firstName("Santiago")
//                .lastName("Cuascota")
//                .documentNumber("1752387504")
//                .build();
//        person1.setCompanyCode(1);
//        person1.setCreatedFromIp("0.0.0.0");
//        person1.setCreatedByUser("FRM0");
//        person1.setStatus(true);
//
//        person2=PersonEntity.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//        person2.setCompanyCode(1);
//        person2.setCreatedFromIp("0.0.0.0");
//        person2.setCreatedByUser("FRM0");
//        person2.setStatus(true);
//
//        person1=entityManager.persist(person1);
//        person2=entityManager.persist(person2);
//
//    }
//
//    @Test
//    public void testFindList() throws Exception {
//        mockMvc.perform(get("/anServices/api/v1/persons").contextPath("/anServices")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$").isArray()).andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].firstName", is(person1.getFirstName())))
//                .andExpect(jsonPath("$[1].firstName", is(person2.getFirstName())))
//                .andDo(MockMvcResultHandlers.print());
//    }
//
//    @Test
//    public void testFindById() throws Exception {
//
//        mockMvc.perform(get("/anServices/api/v1/persons/"+person1.getPersonId()).contextPath("/anServices")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(person1.getFirstName())))
//                .andExpect(jsonPath("lastName", is(person1.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//    }
//
//    @Test
//    public void testSave() throws Exception {
//        PersonVo person=PersonVo.builder()
//                .firstName(faker.name().firstName())
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        mockMvc.perform(post("/anServices/api/v1/persons").contextPath("/anServices")
//                .content(objectMapper.writeValueAsString(person))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(person.getFirstName())))
//                .andExpect(jsonPath("lastName", is(person.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//    }
//
//    @Test
//    public void testUpdate() throws Exception {
//        PersonVo person=PersonVo.builder()
//                .personId(person1.getPersonId())
//                .firstName("SANTY")
//                .lastName(faker.name().lastName())
//                .documentNumber(faker.number().digits(10))
//                .build();
//
//        mockMvc.perform(put("/anServices/api/v1/persons").contextPath("/anServices")
//                .content(objectMapper.writeValueAsString(person))
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("firstName", is(person.getFirstName())))
//                .andExpect(jsonPath("lastName", is(person.getLastName())))
//                .andDo(MockMvcResultHandlers.print());
//    }
//}
