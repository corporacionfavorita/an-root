package ec.com.smx.an.controller;

import ec.com.kruger.spring.ws.controller.test.MockMvcControllerBase;
import ec.com.smx.an.TestConfig;
import ec.com.smx.an.entity.WorkAreaEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * WorkArea Integration Test.
 *
 * @author rzambrano on 2022/02/01.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application.yaml")
@Import(TestConfig.class)
public class WorkAreaIntegrationTest extends MockMvcControllerBase {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private WorkAreaController controller;

    private WorkAreaEntity workAreaEntity;

    @Before
    public void setup() {
        super.setUpBase(controller);
        workAreaEntity = entityManager.persist(
                WorkAreaEntity.builder()
                        .workAreaCode(1)
                        .companyCode(1)
                        .referenceCode(1)
                        .status("ACT")
                        .name("OPERACIONES")
                        .build()
        );
    }

    @Test
    public void testList() throws Exception {
        mockMvc.perform(get("/anServices/api/v1/work_area").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testWorkAreaPositionsList() throws Exception {
        mockMvc.perform(get("/anServices/api/v1/work_area/1/positions").contextPath("/anServices")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
